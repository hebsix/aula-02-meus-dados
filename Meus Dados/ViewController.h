//
//  ViewController.h
//  Meus Dados
//
//  Created by Usuário Convidado on 15/02/17.
//  Copyright © 2017 Gustavo Sales. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    
    __weak IBOutlet UILabel *myLabel1;
    __weak IBOutlet UILabel *myLabel2;
    __weak IBOutlet UILabel *myLabel3;
    
}

- (IBAction)show:(id)sender;
- (IBAction)clear:(id)sender;

@end

