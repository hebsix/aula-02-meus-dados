//
//  ViewController.m
//  Meus Dados
//
//  Created by Usuário Convidado on 15/02/17.
//  Copyright © 2017 Gustavo Sales. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    myLabel1.text = @"Meu nome é ........";
    myLabel2.text = @"Minha idade é .....";
    myLabel3.text = @"Minha cidade é ....";
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)show:(id)sender {
    myLabel1.text = @"Gustavo";
    myLabel2.text = @"22 Anos";
    myLabel3.text = @"São Paulo";
}

- (IBAction)clear:(id)sender {
    myLabel1.text = @"";
    myLabel2.text = @"";
    myLabel3.text = @"";
}
@end
